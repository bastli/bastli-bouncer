from app import create_app, init_notifications
from bot import create_bot

app = create_app()
bot = create_bot(app.config.get('TELEGRAM_BOT_TOKEN'))

init_notifications(app)

if __name__ == '__main__':
    print('Starting telegram bot...', flush=True)
    bot.start_polling()
    print('Starting flask dev server on port 5000...', flush=True)
    try:
        app.run(host='0.0.0.0')
    except BaseException as e:
        print(e)
    bot.stop()

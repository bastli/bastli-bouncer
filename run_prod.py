# wsgi server (used in docker container)
# [bjoern](https://github.com/jonashaag/bjoern) required.

from app import create_app, init_notifications
from bot import create_bot
import bjoern

app = create_app()
bot = create_bot(app.config.get('TELEGRAM_BOT_TOKEN'))

init_notifications(app)

if __name__ == '__main__':
    print('Starting telegram bot...', flush=True)
    bot.start_polling()
    print('Starting bjoern on port 8080...', flush=True)
    try:
        bjoern.run(app, '0.0.0.0', 8080)
    except BaseException as e:
        print(e)
    bot.stop()

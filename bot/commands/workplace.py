from datetime import timedelta
from flask import current_app
from telegram import ParseMode, ReplyKeyboardRemove
from app.controllers import FreeWorkplacesController, \
    UserController, ReservationController, RecordController
from app.exceptions import NoFreeWorkplaceError, \
    ActiveReservationExistsError, ActiveRecordExistsError, \
    NoActiveRecordError, UserBlockedError, UserNotConfirmedError
from app import app_context


def freeworkplaces_command(update, context):
    """Send a message when the command /freeworkplaces is issued."""
    with app_context():
        free_workplaces = FreeWorkplacesController.get_free_workplaces()
    response_text  = '*' + str(free_workplaces) + '* workspaces are currently available.'
    if 'email' in context.user_data:
        response_text += '\n\nYour email address is ' + context.user_data['email']
    update.message.reply_text(response_text,
                              parse_mode=ParseMode.MARKDOWN,
                              reply_markup=ReplyKeyboardRemove())


def reserve_command(update, context):
    """Send a message when the command /reserve is issued."""
    with app_context():
        location_name = current_app.config.get('LOCATION_NAME', 'location')
        user = UserController.get_by_telegram_user_id(update.message.from_user.id)
        if user:
            try:
                reservation = UserController.reserve(user)
                response_text = (
                    'You have a reserved workspace until ' + reservation.time_end.strftime("%H:%M") + '\n'
                    '/cancel reservation or /enter the ' + location_name + '.')
            except NoFreeWorkplaceError:
                response_text = (
                    'There are no workspaces available at the moment. '
                    'Please try again later.')
            except UserNotConfirmedError:
                response_text = (
                    'Your email address ' + user.email + ' is not confirmed yet!\n'
                    'Please confirm your email address first. '
                    'If you didn\'t get an email, /resendconfirmationemail.')
            except UserBlockedError:
                response_text = (
                    'Your account has been blocked.\n'
                    'Please contact an administrator.')
            except ActiveReservationExistsError:
                reservation = ReservationController.get_active_reservation(user)
                response_text = (
                    'You already have a reservation valid until ' + reservation.time_end.strftime("%H:%M") + '\n'
                    '/cancel reservation or /enter the ' + location_name + '.')
            except ActiveRecordExistsError:
                record = RecordController.get_active_record(user)
                response_text = (
                    'You already reported to be at the ' + location_name + '!\n'
                    'Send /leave if you are not there anymore.')
        else:
            response_text = (
                'Your telegram user is not linked to an account!\n'
                'Please /register first and confirm your email address.')

    update.message.reply_text(response_text,
                              parse_mode=ParseMode.MARKDOWN,
                              reply_markup=ReplyKeyboardRemove())


def cancel_command(update, context):
    """Send a message when the command /cancel is issued."""
    with app_context():
        user = UserController.get_by_telegram_user_id(update.message.from_user.id)
        if user:
            reservation = ReservationController.get_active_reservation(user)

            if reservation:
                ReservationController.cancel(reservation)
                response_text = 'Your reservation has been cancelled.'
            else:
                response_text = 'You do not have a reservation!'
        else:
            response_text = (
                'Your telegram user is not linked to an account!\n'
                'Please /register first and confirm your email address.')

    update.message.reply_text(response_text,
                              parse_mode=ParseMode.MARKDOWN,
                              reply_markup=ReplyKeyboardRemove())


def enter_command(update, context):
    """Send a message when the command /enter is issued."""
    with app_context():
        location_name = current_app.config.get('LOCATION_NAME', 'location')
        user = UserController.get_by_telegram_user_id(update.message.from_user.id)
        if user:
            try:
                UserController.start_record(user)
                response_text = (
                    '*GRANTED* - You may enter the ' + location_name + ' now.\n\n'
                    'Remember to send /leave when you leave the '
                    '' + location_name + ' to free your workspace for others.'
                )
            except NoFreeWorkplaceError:
                response_text = (
                    '*DENIED* - There are no workspaces available at the moment. '
                    'Please try again later.')
            except UserBlockedError:
                response_text = (
                    '*DENIED* - Your account has been blocked.\n'
                    'Please contact an administrator.')
            except UserNotConfirmedError:
                response_text = (
                    '*DENIED* - Your email address ' + user.email + ' is not confirmed yet!\n'
                    'Please confirm your email address first. '
                    'If you didn\'t get an email, /resendconfirmationemail.')
            except ActiveRecordExistsError:
                response_text = 'You already reported to be at the ' + location_name + '!'
        else:
            response_text = (
                'Your telegram user is not linked to an account!\n'
                'Please /register first and confirm your email address.')

    update.message.reply_text(response_text,
                              parse_mode=ParseMode.MARKDOWN,
                              reply_markup=ReplyKeyboardRemove())


def leave_command(update, context):
    """Send a message when the command /leave is issued."""
    with app_context():
        location_name = current_app.config.get('LOCATION_NAME', 'location')
        operator_email = current_app.config.get('OPERATOR_EMAIL')
        user = UserController.get_by_telegram_user_id(update.message.from_user.id)
        if user:
            try:
                UserController.terminate_record(user)
                response_text = 'Thank you for the visit. Stay healthy!'
            except NoActiveRecordError:
                response_text = (
                    'You did not report to be at the ' + location_name + '!\n'
                    'If you forgot to report /enter when you entered the ' + location_name + ', '
                    'send an email to ' + operator_email + ' with the time when you were here.')
        else:
            response_text = (
                'Your telegram user is not linked to an account!\n'
                'Please /register first and confirm your email address.')

    update.message.reply_text(response_text,
                              parse_mode=ParseMode.MARKDOWN,
                              reply_markup=ReplyKeyboardRemove())

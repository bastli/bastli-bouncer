from flask import current_app
from telegram import ParseMode, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import CommandHandler, MessageHandler, Filters, ConversationHandler
from validate_email import validate_email
from app.controllers import FreeWorkplacesController, UserController
from app import app_context

# Conversation states
EMAIL, NAME, CONFIRM = range(3)


def create_command(update, context):
    try:
        with app_context():
            user = UserController.get_by_telegram_user_id(update.message.from_user.id)

        if user and user.is_admin:
            response_text = 'Give me the email address.'
            update.message.reply_text(response_text,
                                      reply_markup=ReplyKeyboardRemove())
            return EMAIL
        else:
            response_text = 'You do not have enough permissions for this task.'
            update.message.reply_text(response_text,
                                      reply_markup=ReplyKeyboardRemove())
            context.user_data.clear()
            return ConversationHandler.END
    except:
        return abort_error(update, context)


def email(update, context):
    try:
        email = update.message.text

        with app_context():
            if not validate_email(email_address=email, \
                        check_regex=True, \
                        check_mx=False, \
                        use_blacklist=True, \
                        debug=False):
                response_text = (
                    'Ooops, that doesn\'t seem to be a valid email address. Please try again.\n'
                    '_Send /cancel to abort the account creation process._')
                update.message.reply_text(response_text,
                                          parse_mode=ParseMode.MARKDOWN,
                                          reply_markup=ReplyKeyboardRemove())
                return EMAIL
            elif UserController.get_by_email(email) is not None:
                response_text  = (
                    'Ooops, that email address is already in use. Please give me another one.\n'
                    '_Send /cancel to abort the registration process._')
                update.message.reply_text(response_text,
                                          parse_mode=ParseMode.MARKDOWN,
                                          reply_markup=ReplyKeyboardRemove())
                return EMAIL

        context.user_data['create_email'] = email
        response_text = 'Great! Now give me the name.'
        update.message.reply_text(response_text,
                                  parse_mode=ParseMode.MARKDOWN,
                                  reply_markup=ReplyKeyboardRemove())
        return NAME
    except:
        return abort_error(update, context)


def name(update, context):
    try:
        name = update.message.text

        with app_context():
            if len(name) < 3:
                response_text = (
                    'Ooops, that name seems to be a little too short! Please give me the full name.\n'
                    '_Send /cancel to abort the registration process._')
                update.message.reply_text(response_text,
                                          parse_mode=ParseMode.MARKDOWN,
                                          reply_markup=ReplyKeyboardRemove())
                return NAME

        context.user_data['create_name'] = name
        return send_confirm_message(update, context)
    except:
        return abort_error(update, context)


def send_confirm_message(update, context):
    update_text = None

    update_text = (
            'I will create a new account with the name "' + context.user_data['create_name'] + '" '
            'and the email address "' + context.user_data['create_email'] + '".')

    response_text = (
        'Great!\n\n' + update_text + '\n\n'
        'Is that okay?')
    reply_keyboard = [['Yes', 'No']]
    update.message.reply_text(response_text,
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False))
    return CONFIRM


def confirm(update, context):
    try:
        reply = update.message.text
        if reply == 'Yes':
            email = context.user_data['create_email'] if 'create_email' in context.user_data else None
            name = context.user_data['create_name'] if 'create_name' in context.user_data else None

            with app_context():
                UserController.create_admin(name, email)

                response_text = (
                    'Successfully created the account.\n\n'
                    'Instructions on how to set a password '
                    'have been sent to the given email address.'
                )
        else:
            response_text = (
                'I did not create a new account.\n\n'
                'Restart the registration process with /register.'
            )

        update.message.reply_text(response_text, reply_markup=ReplyKeyboardRemove())
        context.user_data.clear()
        return ConversationHandler.END
    except Exception as e:
        raise e
        return abort_error(update, context)


def cancel(update, context):
    update.message.reply_text('The account creation process has been cancelled.',
                              reply_markup=ReplyKeyboardRemove())
    context.user_data.clear()
    return ConversationHandler.END


def abort_error(update, context):
    update.message.reply_text((
        'Ooops, something went horribly wrong on my side.\n'
        'The account creation had to be aborted.\n'
        'Restart the process with /create.'),
        reply_markup=ReplyKeyboardRemove())
    context.user_data.clear()
    return ConversationHandler.END


create_conv_handler = ConversationHandler(
    entry_points=[CommandHandler('create', create_command)],
    states={
        EMAIL: [MessageHandler(Filters.regex('^(?!/cancel).*$'), email)],
        NAME: [MessageHandler(Filters.regex('^(?!/cancel).*$'), name)],
        CONFIRM: [MessageHandler(Filters.regex('^(?!/cancel).*$'), confirm)],
    },
    fallbacks=[CommandHandler('cancel', cancel)]
)

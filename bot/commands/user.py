from telegram import ParseMode, ReplyKeyboardRemove
from flask import current_app
from app.controllers import UserController, ReservationController, RecordController
from app import app_context


def resetpassword_command(update, context):
    """Send a message when the command /resetpassword is issued."""
    with app_context():
        user = UserController.get_by_telegram_user_id(update.message.from_user.id)
        if user:
            password_reset_link = UserController.get_password_reset_link(user)
            response_text = '[Click here](' + password_reset_link + ') to set a new password.'
        else:
            response_text = (
                'You telegram user is not associated with an account!\n'
                'Please /register first.')
    update.message.reply_text(response_text,
                              parse_mode=ParseMode.MARKDOWN,
                              reply_markup=ReplyKeyboardRemove())


def resendconfirmationemail_command(update, context):
    """Send a message when the command /resendconfirmationemail is issued."""
    with app_context():
        user = UserController.get_by_telegram_user_id(update.message.from_user.id)
        if user:
            if user.is_confirmed:
                response_text = 'Your email address is already confirmed!'
            else:
                UserController.send_confirm_email(user)
                response_text = (
                    'An email with instructions have been sent to ' + user.email + '.\n'
                    'Please check your inbox.')
        else:
            response_text = (
                'You telegram user is not associated with an account!\n'
                'Please /register first.')
    update.message.reply_text(response_text,
                              parse_mode=ParseMode.MARKDOWN,
                              reply_markup=ReplyKeyboardRemove())


def status_command(update, context):
    """Send a message when the command /status is issued."""
    with app_context():
        location_name = current_app.config.get('LOCATION_NAME', 'location')
        user = UserController.get_by_telegram_user_id(update.message.from_user.id)
        if user:
            response_text = (
                'Account of ' + user.name + ' (' + user.email + ')\n\n')
            if not user.is_confirmed:
                response_text += (
                    'Your account is locked until the email address is confirmed!\n\n'
                    'Request another confirmation email with /resendconfirmationemail.')
            else:
                reservation = ReservationController.get_active_reservation(user)
                record = RecordController.get_active_record(user)

                if record:
                    response_text += (
                        'You have reported to be at the ' + location_name + '.\n'
                        'Do you want to make the workspace free for others and /leave?')
                elif reservation:
                    response_text += (
                        'You have reserved a workspace until ' + reservation.time_end.strftime("%H:%M") + '\n'
                        '/cancel reservation or /enter the ' + location_name + '.')
                else:
                    response_text += (
                        'It\'s really boring here!\n\n'
                        'Do you want to /reserve a workplace or /enter the ' + location_name + '?')
            
            if user.is_admin:
                active_records = RecordController.get_all_active_records()
                active_reservations = ReservationController.get_all_active_reservations()

                response_text += '\n\nACTIVE RECORDS\n'
                if len(active_records) > 0:
                    for record in active_records:
                        if record.user.telegram_id:
                            name = '[' + record.user.name + '](tg://user?id=' + record.user.telegram_id + ')'
                        else:
                            name = record.user.name
                        response_text += '  - ' + name + ' (started at ' + record.time_start.strftime('%H:%M') + ')\n'
                        response_text += '    ' + record.user.email + '\n'
                else:
                    response_text += '  none\n'

                response_text += '\nACTIVE RESERVATIONS\n'
                if len(active_reservations) > 0:
                    for reservation in active_reservations:
                        if reservation.user.telegram_id:
                            name = '[' + reservation.user.name + '](tg://user?id=' + reservation.user.telegram_id + ')'
                        else:
                            name = reservation.user.name
                        response_text += '  - ' + name + ' (valid until at ' + reservation.time_end.strftime('%H:%M') + ')\n'
                        response_text += '    ' + reservation.user.email + '\n'
                else:
                    response_text += '  none\n'
        else:
            response_text = (
                'You telegram user is not associated with an account!\n'
                'Please /register first.')

    update.message.reply_text(response_text,
                              parse_mode=ParseMode.MARKDOWN,
                              reply_markup=ReplyKeyboardRemove())

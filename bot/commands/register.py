from flask import current_app
from telegram import ParseMode, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import CommandHandler, MessageHandler, Filters, ConversationHandler
from validate_email import validate_email
from app.controllers import FreeWorkplacesController, UserController
from app import app_context

# Conversation states
REGISTER_TYPE, EMAIL, NAME, CONFIRM, PASSWORD = range(5)


def register_command(update, context):
    try:
        with app_context():
            user = UserController.get_by_telegram_user_id(update.message.from_user.id)
            registration_enabled = current_app.config.get('REGISTRATION_ENABLED', False)
            account_update_allowed = current_app.config.get('ACCOUNT_UPDATE_ALLOWED', False)

        if user:
            response_text = 'Your telegram user is already linked to an existing account (' + user.email + ').\n\n'

            if account_update_allowed:
                if user.has_password:
                    response_text += (
                        'You can update the account information or '
                        'unlink your telegram user from the account.\n')
                    reply_keyboard = [['Update', 'Unlink', 'Cancel']]
                else:
                    response_text += (
                        'You can update the account information instead.\n'
                        'To unlink your telegram user from the account, '
                        'you must set a password first.')
                    reply_keyboard = [['Update', 'Set Password', 'Cancel']]
            else:
                if user.has_password:
                    response_text += (
                        'You can unlink your telegram user from the account.\n')
                    reply_keyboard = [['Unlink', 'Cancel']]
                else:
                    response_text += (
                        'To unlink your telegram user from the account, '
                        'you must set a password first.')
                    reply_keyboard = [['Set Password', 'Cancel']]

            update.message.reply_text(response_text,
                                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False))
            return REGISTER_TYPE
        elif registration_enabled:
            update.message.reply_text(
                'I will guide you through the interactive registration process now.\n'
                'Send /cancel to abort the registration at any time.\n\n'
                'What is your email address?')
            return EMAIL
        else:
            response_text = (
                'Registrations is disabled.\n\n'
                'Please ask an administrator for an account.')
            update.message.reply_text(response_text,
                                      reply_markup=ReplyKeyboardRemove())
            context.user_data.clear()
            return ConversationHandler.END
    except:
        return abort_error(update, context)


def register_type(update, context):
    try:
        reply = update.message.text
        with app_context():
            user = UserController.get_by_telegram_user_id(update.message.from_user.id)
            registration_enabled = current_app.config.get('REGISTRATION_ENABLED', False)
            account_update_allowed = current_app.config.get('ACCOUNT_UPDATE_ALLOWED', False)

            if reply == 'Update' and user and account_update_allowed:
                context.user_data['register_update'] = True

                response_text = (
                    'Send /cancel to abort the update process at any time. '
                    'The data will not be updated until the end of the process.\n\n'
                    'The stored email address is *' + user.email + '*.\n'
                    'Send me the new email address or /skip if it is still up to date.')
                update.message.reply_text(response_text,
                                        parse_mode=ParseMode.MARKDOWN,
                                        reply_markup=ReplyKeyboardRemove())
                return EMAIL
            elif reply == 'Unlink':
                if user:
                    UserController.unlink_telegram_user_id(update.message.from_user.id)
                    response_text = (
                        'Your are free! Now, let\'s continue with the registration.\n'
                        'Send /cancel to abort the registration at any time.\n\n'
                        'What is your email address?')
                    update.message.reply_text(response_text,
                                            parse_mode=ParseMode.MARKDOWN,
                                            reply_markup=ReplyKeyboardRemove())
                    return EMAIL
                else:
                    response_text = (
                        'Ooops, there\'s something wrong.\n'
                        'I could not find any information about your account anymore.'
                        'Send /register to start the registration process again.')
                    update.message.reply_text(response_text,
                                            parse_mode=ParseMode.MARKDOWN,
                                            reply_markup=ReplyKeyboardRemove())
                    context.user_data.clear()
                    return ConversationHandler.END
            elif reply == 'Set Password':
                user = UserController.get_by_telegram_user_id(update.message.from_user.id)
                if user:
                    password_reset_link = UserController.get_password_reset_link(user)
                    print(password_reset_link)
                    response_text = (
                        '[Click here](' + password_reset_link + ') to set a new password.\n\n'
                        'Send /register afterwards to start the registration process again.')
                else:
                    response_text = (
                        'Ooops, there\'s something wrong.\n'
                        'I could not find any information about your account anymore.'
                        'Send /register to start the registration process again.')
                update.message.reply_text(response_text,
                                          parse_mode=ParseMode.MARKDOWN,
                                          reply_markup=ReplyKeyboardRemove())
                context.user_data.clear()
                return ConversationHandler.END
            elif reply == 'Cancel':
                return cancel(update, context)
            else:
                update.message.reply_text('I did not understand your answer.\n'
                                          'Please choose an option from the reply keyboard.')
                return REGISTER_TYPE
    except:
        return abort_error(update, context)


def email_skip(update, context):
    try:
        if 'register_update' in context.user_data:
            with app_context():
                user = UserController.get_by_telegram_user_id(update.message.from_user.id)
            response_text = (
                'Great! I know you as *' + user.name + '*.\n'
                'Send me another name or /skip if you don\'t want to change it.')
            update.message.reply_text(response_text,
                                      parse_mode=ParseMode.MARKDOWN,
                                      reply_markup=ReplyKeyboardRemove())
            return NAME
        else:
            response_text = (
                'You prankster, you! There is no way to skip this step!\n'
                'Tell me your email address or send /cancel to abort the registration.')
            update.message.reply_text(response_text,
                                      parse_mode=ParseMode.MARKDOWN,
                                      reply_markup=ReplyKeyboardRemove())
            return EMAIL
    except:
        return abort_error(update, context)

def email(update, context):
    try:
        email = update.message.text

        with app_context():
            if not validate_email(email_address=email, \
                        check_regex=True, \
                        check_mx=False, \
                        use_blacklist=True, \
                        debug=False):
                response_text = (
                    'Ooops, that doesn\'t seem to be a valid email address. Please try again.\n'
                    '_Send /cancel to abort the registration process._')
                update.message.reply_text(response_text,
                                          parse_mode=ParseMode.MARKDOWN,
                                          reply_markup=ReplyKeyboardRemove())
                return EMAIL
            elif UserController.get_by_email(email) is not None:
                response_text  = (
                    'Ooops, your email address is already taken. Please take another one.\n'
                    '_Send /cancel to abort the registration process._')
                update.message.reply_text(response_text,
                                          parse_mode=ParseMode.MARKDOWN,
                                          reply_markup=ReplyKeyboardRemove())
                return EMAIL

        context.user_data['register_email'] = email

        if 'register_update' in context.user_data:
            user = UserController.get_by_telegram_user_id(update.message.from_user.id)
            response_text = (
                'Great! I know you as *' + user.name + '*.\n'
                'Send me another name or /skip if you don\'t want to change it.')
        else:
            response_text = 'Great! What\'s your name?'

        update.message.reply_text(response_text,
                                  parse_mode=ParseMode.MARKDOWN,
                                  reply_markup=ReplyKeyboardRemove())
        return NAME
    except:
        return abort_error(update, context)


def name_skip(update, context):
    try:
        if 'register_update' in context.user_data:
            return send_confirm_message(update, context)
        else:
            response_text = (
                'You prankster, you! There is no way to skip this step!\n'
                'Tell me your name or send /cancel to abort the registration.')
            update.message.reply_text(response_text,
                                      reply_markup=ReplyKeyboardRemove())
            return NAME
    except:
        return abort_error(update, context)

def name(update, context):
    try:
        name = update.message.text

        with app_context():
            if len(name) < 3:
                response_text = (
                    'Ooops, that name seems to be a little too short! Please give me your full name.\n'
                    '_Send /cancel to abort the registration process._')
                update.message.reply_text(response_text,
                                          parse_mode=ParseMode.MARKDOWN,
                                          reply_markup=ReplyKeyboardRemove())
                return NAME

        context.user_data['register_name'] = name

        return send_confirm_message(update, context)
    except:
        return abort_error(update, context)


def send_confirm_message(update, context):
    update_text = None

    if 'register_update' in context.user_data:
        if 'register_name' in context.user_data:
            update_text = 'I will change the name to "' + context.user_data['register_name'] + '"'
        if 'register_email' in context.user_data:
            if update_text:
                update_text += ' and the associated email address to "' + context.user_data['register_email'] + '"'
            else:
                update_text = 'I will change the email address to "' + context.user_data['register_email'] + '"'
        
        if not update_text:
            response_text = (
                'Great! There is nothing to update!\n'
                'Thank you for reviewing the account information.')
            update.message.reply_text(response_text,
                                    parse_mode=ParseMode.MARKDOWN,
                                    reply_markup=ReplyKeyboardRemove())
            context.user_data.clear()
            return ConversationHandler.END
        else:
            update_text += '.'
    else:
        update_text = (
            'I will create a new account with the name "' + context.user_data['register_name'] + '" '
            'and the email address "' + context.user_data['register_email'] + '".')

    response_text = (
        'Great!\n\n' + update_text + '\n\n'
        'Is that okay?')
    reply_keyboard = [['Yes', 'No']]
    update.message.reply_text(response_text,
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False))
    return CONFIRM


def confirm(update, context):
    try:
        reply = update.message.text
        if reply == 'Yes':
            telegram_chat_id = update.message.chat_id
            telegram_user_id = update.message.from_user.id
            email = context.user_data['register_email'] if 'register_email' in context.user_data else None
            name = context.user_data['register_name'] if 'register_name' in context.user_data else None

            with app_context():
                if 'register_update' in context.user_data:
                    user = UserController.get_by_telegram_user_id(telegram_user_id)

                    if user:
                        UserController.update(user, name, email, telegram_chat_id)
                        response_text = 'Successfully updated the account information.'
                        if 'register_email' in context.user_data:
                            response_text += (
                                '\n\nYour account has been locked '
                                'until you confirm the new email address.\n'
                                'Please check your inbox.')
                        update.message.reply_text(response_text, reply_markup=ReplyKeyboardRemove())
                        context.user_data.clear()
                        return ConversationHandler.END
                    else:
                        return abort_error(update, context)
                else:
                    UserController.create(name, email, telegram_user_id, telegram_chat_id)

                    response_text = (
                        'Successfully created your account.\n\n'
                        'Do you want to set a password for your account?')
                    reply_keyboard = [['Yes', 'No']]
                    update.message.reply_text(response_text,
                        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False))
                    return PASSWORD
        else:
            if 'register_update' in context.user_data:
                response_text = 'I did not change any account information. Aborted.'
            else:
                response_text = (
                    'I did not create a new account.\n\n'
                    'Restart the registration process with /register.')

            update.message.reply_text(response_text, reply_markup=ReplyKeyboardRemove())
            context.user_data.clear()
            return ConversationHandler.END
    except:
        return abort_error(update, context)


def password(update, context):
    try:
        reply = update.message.text
        if reply.startswith('/cancel'):
            return
        if reply == 'Yes':
            with app_context():
                user = UserController.get_by_telegram_user_id(update.message.from_user.id)
                password_reset_link = UserController.get_password_reset_link(user)
                response_text = (
                    '[Click here](' + password_reset_link + ') to set a password.\n\n'
                    '_You have reached the end of the registration process._')
        else:
            response_text = 'You have reached the end of the registration process.'

        update.message.reply_text(response_text,
                                  parse_mode=ParseMode.MARKDOWN,
                                  reply_markup=ReplyKeyboardRemove())
        context.user_data.clear()
        return ConversationHandler.END 
    except:
        return abort_error(update, context)


def cancel(update, context):
    update.message.reply_text('The registration process has been cancelled.',
                              reply_markup=ReplyKeyboardRemove())
    context.user_data.clear()
    return ConversationHandler.END


def abort_error(update, context):
    update.message.reply_text((
        'Ooops, something went horribly wrong on my side.\n'
        'The registration had to be aborted.\n'
        'Restart the registration with /register.'),
        reply_markup=ReplyKeyboardRemove())
    context.user_data.clear()
    return ConversationHandler.END


register_conv_handler = ConversationHandler(
    entry_points=[CommandHandler('register', register_command)],
    states={
        # REGISTER_TYPE: [MessageHandler(Filters.regex('^(Update|Set Password|Unlink|Cancel)$'), gender)],
        REGISTER_TYPE: [MessageHandler(Filters.regex('^(?!/cancel).*$'), register_type)],
        EMAIL: [CommandHandler('skip', email_skip),
                MessageHandler(Filters.regex('^(?!/cancel).*$'), email)],
        NAME: [CommandHandler('skip', name_skip),
               MessageHandler(Filters.regex('^(?!/cancel).*$'), name)],
        CONFIRM: [MessageHandler(Filters.regex('^(?!/cancel).*$'), confirm)],
        PASSWORD: [MessageHandler(Filters.regex('^(?!/cancel).*$'), password)]
    },
    fallbacks=[CommandHandler('cancel', cancel)]
)

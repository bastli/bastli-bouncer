from telegram import ParseMode, ReplyKeyboardRemove
from flask import current_app
from app.controllers import UserController
from app.exceptions import InvalidUserTokenError, TelegramUserAlreadyLinkedError
from app import app_context


def start_command(update, context):
    """Send a message when the command /start is issued."""
    name = update.message.from_user.first_name

    with app_context():
        bot_name = current_app.config.get('TELEGRAM_BOT_NAME', 'Bastli Bouncer bot')
        location_name = current_app.config.get('LOCATION_NAME', 'location')

    start_text  = (
        'Hi ' + name + '!\n\n'
        'I\'m the ' + bot_name + '.\n'
        'My job is to ensure that the ' + location_name + ' doesn\'t get crowded and to maintain the user list.\n\n'
        'Please follow the rules below:\n\n'
        '*Rule 1*\n'
        'Register yourself with /register.\n\n'
        '*Rule 2*\n'
        'Check for free workplaces and /reserve one before you come here.\n\n'
        '*Rule 3*\n'
        'Send /enter before you enter the ' + location_name + ' and wait for clearance.\n\n'
        '*Rule 4*\n'
        'Send /leave when you leave the ' + location_name +'.\n\n\n'
        'See /help for more information about the commands.'
    )
    update.message.reply_text(start_text,
                              parse_mode=ParseMode.MARKDOWN,
                              reply_markup=ReplyKeyboardRemove())

    start_data = update.message.text.split(' ')

    if len(start_data) == 3:
        email = start_data[1]
        token = start_data[2]
        telegram_chat_id = update.message.chat_id
        telegram_user_id = update.message.from_user.id

        with app_context():
            try:
                UserController.link_telegram(email, token, telegram_user_id, telegram_chat_id)
                response_text = 'Your Telegram user has been linked to the account ' + email
            except InvalidUserTokenError:
                response_text = (
                    'Ooops, the given token was invalid.\n'
                    'Please try again with an updated link on the website.')
            except TelegramUserAlreadyLinked:
                response_text = 'Ooops, your Telegram user seems to be linked already to another account!'
        
        update.message.reply_text(response_text,
                                  reply_markup=ReplyKeyboardRemove())


def help_command(update, context):
    """Send a message when the command /help is issued."""
    with app_context():
        bot_name = current_app.config.get('TELEGRAM_BOT_NAME', 'Bastli Bouncer bot')

    help_text  = (
        '*' + bot_name + ' usage:*\n\n'
        '/freeworkplaces\n'
        '    _Get available workplaces._\n'
        '/register\n'
        '    _Initiate account creation._\n'
        '/resetpassword\n'
        '    _Request password reset._\n'
        '/status\n'
        '    _Get status about own user._\n'
        '/reserve\n'
        '    _Reserve a workplace._\n'
        '/cancel\n'
        '    _Cancel a workplace reservation._\n'
        '/enter\n'
        '    _Request for entry._\n'
        '/leave\n'
        '    _Free the workspace._\n'
    )

    with app_context():
        user = UserController.get_by_telegram_user_id(update.message.from_user.id)

        if user and user.is_admin:
            help_text += (
                '\n--------------\n'
                'ADMIN COMMANDS\n\n'
                '/create\n'
                '    _Create a new account._'
            )

    update.message.reply_text(help_text,
                              parse_mode=ParseMode.MARKDOWN,
                              reply_markup=ReplyKeyboardRemove())

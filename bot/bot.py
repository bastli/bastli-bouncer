from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

from .commands.help import help_command, start_command
from .commands.workplace import freeworkplaces_command, reserve_command, cancel_command, enter_command, leave_command
from .commands.user import status_command, resetpassword_command, resendconfirmationemail_command
from .commands.register import register_conv_handler
from .commands.admin.create import create_conv_handler
from .jobs import JobQueue


def create_bot(token):
    updater = Updater(token, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    dp.add_handler(CommandHandler('start', start_command))
    dp.add_handler(CommandHandler('help', help_command))
    dp.add_handler(CommandHandler('freeworkplaces', freeworkplaces_command))
    dp.add_handler(CommandHandler('resetpassword', resetpassword_command))
    dp.add_handler(CommandHandler('resendconfirmationemail', resendconfirmationemail_command))
    dp.add_handler(CommandHandler('status', status_command))
    dp.add_handler(CommandHandler('reserve', reserve_command))
    dp.add_handler(CommandHandler('cancel', cancel_command))
    dp.add_handler(CommandHandler('enter', enter_command))
    dp.add_handler(CommandHandler('leave', leave_command))
    dp.add_handler(register_conv_handler)
    dp.add_handler(create_conv_handler)

    JobQueue.set_queue(updater.job_queue)

    return updater

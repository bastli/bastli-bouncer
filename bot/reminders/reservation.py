from datetime import datetime, timedelta
from telegram import ParseMode, ReplyKeyboardRemove
from flask import current_app
from app import app_context
from ..jobs import JobQueue


def add_reservation_reminder(reservation):
    with app_context():
        reminder_time_delta = current_app.config.get('RESERVATION_REMINDER_TIME_DELTA', timedelta(minutes=11))
        JobQueue.add_job_once(
            modelname='reservation',
            id=reservation._id,
            taskname='',
            callback=send_reservation_reminder,
            when=(reservation.time_end - reminder_time_delta - datetime.now()),
            context={ 'reservation_id': reservation._id }
        )


def remove_reservation_reminder(reservation):
    JobQueue.remove_job(modelname='reservation', id=reservation._id, taskname='')


def update_reservation_reminder(reservation):
    remove_reservation_reminder(reservation)
    add_reservation_reminder(reservation)


def send_reservation_reminder(context):
    job = context.job
    reservation_id = job.context['reservation_id']

    from app.controllers import ReservationController

    with app_context():
        location_name = current_app.config.get('LOCATION_NAME', 'our location')
        reservation = ReservationController.get(reservation_id)
        chat_id = reservation.user.telegram_chat_id
        now = datetime.now()
        seconds = (reservation.time_end - now).total_seconds()

        hours, seconds = divmod(seconds, 3600)
        minutes, seconds = divmod(seconds, 60)
        if hours > 0:
            if minutes > 0:
                time_string = '{} hours and {} minutes'.format(int(hours), int(minutes))
            else:
                time_string = '{} hours'.format(int(hours))
        elif minutes > 0:
            time_string = '{} minutes'.format(int(minutes))
        else:
            time_string = '{} seconds'.format(int(seconds))

    reminder_text = (
        'Your reservation expires in ' + time_string + '.\n\n'
        'Send /reserve to renew your reservation if you are still on the way to ' + location_name + '.'
    )
    context.bot.send_message(chat_id, text=reminder_text)

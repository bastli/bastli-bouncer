# User-defined exceptions

class Error(Exception):
    """Base class for other exceptions"""
    pass


class JobQueueNotInitializedError(Error):
    """Raised when JobQueue instance is not ready yet."""
    pass

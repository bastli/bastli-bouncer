from .exceptions import JobQueueNotInitializedError

class JobQueue(object):

    @classmethod
    def set_queue(cls, job_queue):
        cls.job_queue = job_queue


    @classmethod
    def add_job_once(cls, modelname, id, taskname, callback, when, context=None):
        cls._check_job_queue()

        name = cls._generate_job_name(modelname, id, taskname)
        cls.job_queue.run_once(
            callback,
            when=when,
            context=context,
            name=name
        )


    @classmethod
    def add_job_repeating(cls, modelname, id, taskname, callback, interval, first=None, context=None):
        cls._check_job_queue()

        name = cls._generate_job_name(modelname, id, taskname)
        cls.job_queue.run_repeating(
            callback,
            interval=interval,
            first=first,
            context=context,
            name=name
        )


    @classmethod
    def remove_job(cls, modelname, id, taskname):
        cls._check_job_queue()

        name = cls._generate_job_name(modelname, id, taskname)
        jobs = cls.job_queue.get_jobs_by_name(name)

        for i in range(len(jobs)):
            jobs[i].schedule_removal()


    @classmethod
    def _check_job_queue(cls):
        if cls.job_queue is None:
            raise JobQueueNotInitializedError

    @staticmethod
    def _generate_job_name(modelname, id, taskname):
        return modelname + str(id) + taskname

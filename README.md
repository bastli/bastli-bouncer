# Bastli Bouncer

This is an attendance manager/tracker to ensure COVID-19 restrictions.

## Development

**IMPORTANT**: DO NOT CHANGE already commited migrations files!

**Hint**: Development for the telegram bot requires to build and run the `prod` variant!


All files related to data management and flask are in `./app`.
The files for the telegram bot are in `./bot`.

Use the script `manage.sh` for local development.

```shell
$ ./manage.sh help

Management Script Usage:

  manage.sh [COMMAND]

COMMAND:
  build [dev|prod]               Build docker image for given environment.
  run [dev|prod]                 Run docker container for given environment.
  services [start|restart|stop]  Start/stop service dependencies.
  makemigrations                 Create new migration files.
  migrate                        Apply migrations to local database.
  update_dependencies            Update dependencies based in requirements.in.
```

### Example workflow

Build local development image:

```shell
./manage.sh build dev
```

Start service dependencies:

```shell
./manage.sh services start
```

Apply database migrations:

```shell
./manage.sh migrate
```

Run development server:

```shell
./manage.sh run dev
```

Start developing now.

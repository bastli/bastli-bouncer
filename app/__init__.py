from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap
from flask_wtf.csrf import CSRFProtect
from flask_mail import Mail
from .app_context import AppContext

# global variable initialization
db = SQLAlchemy()
mail = Mail()
csrf = CSRFProtect()
appctx = AppContext()

def app_context():
    return appctx.app_context()


def init_notifications(app):
    from .controllers import ReservationController, RecordController

    with app.app_context():
        ReservationController.initialize_notifications()
        RecordController.initialize_notifications()


def create_app():
    # initialize flask app and configure
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_pyfile('config.py')

    # initialize AppContext for usage outside the flask application
    appctx.init_app(app)

    # initialize Mail
    mail.init_app(app)

    # initialize CSRF protection
    csrf.init_app(app)

    # initialize ORM
    db.init_app(app)

    # add database migration from flask-migrate
    Migrate(app, db)

    # add flask-bootstrap
    Bootstrap(app)

    from app import models
    from app import controllers

    from .errorpages import create_error_pages

    create_error_pages(app)

    # add all blueprints
    from .login import login_bp
    app.register_blueprint(login_bp)
    from .bouncer import bouncer_bp
    app.register_blueprint(bouncer_bp)
    from .admin import admin_bp
    app.register_blueprint(admin_bp, url_prefix='/admin')

    return app

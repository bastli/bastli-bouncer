from flask import current_app, flash, redirect, render_template, url_for, request, abort, make_response

from ..controllers.user import UserController
from ..exceptions import UserRegistrationInvalidDataError, UserBlockedError
from ..auth import login as auth_login, logout as auth_logout, is_authenticated
from . import login_bp

@login_bp.route('/register', methods=['GET', 'POST'])
def register():
    if is_authenticated():
        return redirect(url_for('bouncer.home'))

    registration_enabled = current_app.config.get('REGISTRATION_ENABLED', False)
    if not registration_enabled:
        flash('Registration is disabled.', 'warning')
        return redirect(url_for('bouncer.home'))

    errors = None
    if request.method == 'POST':
        try:
            UserController.check_user_data( \
                name=request.form['name'], \
                email=request.form['email'], \
                password=request.form['password'], \
                password2=request.form['password2'])
            UserController.create( \
                name=request.form['name'], \
                email=request.form['email'], \
                password=request.form['password'])
            
            return make_response(render_template('login/register_success.html', title='Registration'))

        except UserRegistrationInvalidDataError as e:
            errors = e.errors
            flash('Some fields have errors: ' + ', '.join(errors), 'error')
    return make_response(render_template('login/register.html', title='Registration', errors=errors))


@login_bp.route('/confirm/email')
def confirm_email():
    email = request.args.get('email')
    token = request.args.get('token')
    action = request.args.get('action')
    if action == 'confirm':
        if not UserController.confirm_email(email, token):
            abort(404)
        return make_response(render_template('login/confirm_email_confirmed.html', title='Email Address confirmed'))
    elif not UserController.verify_confirm_email(email, token):
        abort(404)

    return make_response(render_template('login/confirm_email.html', title='Confirm Email Address', email=email, token=token))


@login_bp.route('/login', methods=['GET', 'POST'])
def login():
    if is_authenticated():
        return redirect(url_for('bouncer.home'))

    if request.method == 'POST':
        try:
            auth_login(request.form['email'], request.form['password'])
            return redirect(url_for('bouncer.home'))
        except UserBlockedError:
            flash('Account has been blocked. Please contact an administrator.', 'error')
        except:
            flash('Invalid Credentials. Please try again.', 'error')
    return make_response(render_template('login/login.html', title='Login'))


@login_bp.route('/password/forgotten', methods=['GET', 'POST'])
def password_forgotten():
    if is_authenticated():
        return redirect(url_for('bouncer.home'))

    errors = None
    if request.method == 'POST':
        user = UserController.get_by_email(request.form['email'])
        if user:
            UserController.send_password_reset_email(user)
        flash('Instructions on how to reset the password has been sent the your email inbox.')
        return redirect(url_for('login.login'))

    return make_response(render_template('login/password_forgotten.html', title='Forgotten Password'))


@login_bp.route('/password/reset', methods=['GET', 'POST'])
def password_reset():
    email = request.args.get('email')
    token = request.args.get('token')

    if not UserController.verify_confirm_email(email, token):
        abort(404)

    if request.method == 'POST':
        if request.form['password'] is not None and \
            len(request.form['password']) > 0 and \
                request.form['password'] == request.form['password2']:
            if UserController.reset_password(email, token, request.form['password']):
                flash('Password has been reset! Please try to log in with the new password.')
                return redirect(url_for('login.login'))
            else:
                flash('Could not reset password. An unknown error has occured.', 'error')
        else:
            flash('Passwords do not match.', 'error')
    return make_response(render_template('login/password_reset.html', title='Password Reset'))


@login_bp.route('/password/set', methods=['GET', 'POST'])
def password_set():
    email = request.args.get('email')
    token = request.args.get('token')

    if not UserController.verify_confirm_email(email, token):
        abort(404)

    if request.method == 'POST':
        if request.form['password'] is not None and \
            len(request.form['password']) > 0 and \
                request.form['password'] == request.form['password2']:
            if UserController.reset_password(email, token, request.form['password'], confirm_email=True):
                flash('Password has been reset! Please try to log in with the new password.')
                return redirect(url_for('login.login'))
            else:
                flash('Could not reset password. An unknown error has occured.', 'error')
        else:
            flash('Passwords do not match.', 'error')
    return make_response(render_template('login/password_reset.html', title='Password Reset'))



@login_bp.route('/logout')
def logout():
    if auth_logout():
        return make_response(render_template('login/logout.html', title='Logout'))
    return redirect(url_for('bouncer.home'))

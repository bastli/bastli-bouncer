class AppContext(object):

    def init_app(self, new_app):
        self.app = new_app

    def app_context(self):
        return self.app.app_context()

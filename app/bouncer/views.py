from urllib.parse import quote_plus
from flask import flash, current_app, redirect, render_template, url_for, request, abort, make_response, session

from . import bouncer_bp
from ..auth import login_required, is_authenticated, get_authenticated_user
from ..controllers import FreeWorkplacesController, UserController, ReservationController, RecordController
from ..exceptions import NoFreeWorkplaceError, UserNotConfirmedError, UserBlockedError

@bouncer_bp.route('/', methods=['GET', 'POST'])
def home():
    free_workplaces = FreeWorkplacesController.get_free_workplaces()
    registration_enabled = current_app.config.get('REGISTRATION_ENABLED', False)

    if is_authenticated():
        user = get_authenticated_user()
        reservation = ReservationController.get_active_reservation(user)
        record = RecordController.get_active_record(user)

        if request.method == 'POST':
            if request.form['action'] == 'resendconfirmation':
                try:
                    UserController.send_confirm_email(user)
                    flash('An email with instructions has been sent to ' + user.email + '. Please check your inbox.')
                except:
                    flash('There was an unknown error while sending the confirmation email. Please try again later.', 'error')
            if request.form['action'] == 'enter':
                try:
                    UserController.start_record(user)
                    flash('You are allowed to enter.')
                    return redirect(url_for('bouncer.home'))
                except UserNotConfirmedError:
                    flash('Your account is locked until your email address has been confirmed.', 'error')
                except UserBlockedError:
                    flash('Your account is blocked. Please contact an administrator.', 'error')
                except NoFreeWorkplaceError:
                    flash('No free workspaces available! Try again later.', 'error')
            if request.form['action'] == 'reserve':
                try:
                    UserController.reserve(user)
                    flash('Your reservation has been recorded. Your request for entry will be granted immediately as long as your reservation is valid.')
                    return redirect(url_for('bouncer.home'))
                except UserNotConfirmedError:
                    flash('Your account is locked until your email address has been confirmed.', 'error')
                except NoFreeWorkplaceError:
                    flash('No free workspaces available! Try again later.', 'error')

        return make_response(render_template('bouncer/home_authenticated.html',
                                             title='Home',
                                             free_workplaces=free_workplaces,
                                             location_name=current_app.config.get('LOCATION_NAME', 'our location'),
                                             application_name=current_app.config.get('APPLICATION_NAME', 'Bouncer Service'),
                                             operator_website=current_app.config.get('OPERATOR_WEBSITE'),
                                             operator_name=current_app.config.get('OPERATOR_NAME'),
                                             operator_email=current_app.config.get('OPERATOR_EMAIL'),
                                             user=user,
                                             reservation=reservation,
                                             record=record))
    return make_response(render_template('bouncer/home_anonymous.html',
                                         title='Home',
                                         registration_enabled=registration_enabled,
                                         free_workplaces=free_workplaces,
                                         location_name=current_app.config.get('LOCATION_NAME', 'our location'),
                                         application_name=current_app.config.get('APPLICATION_NAME', 'Bouncer Service'),
                                         operator_website=current_app.config.get('OPERATOR_WEBSITE'),
                                         operator_name=current_app.config.get('OPERATOR_NAME'),
                                         operator_email=current_app.config.get('OPERATOR_EMAIL')))


@bouncer_bp.route('/link_telegram')
@login_required
def link_telegram():
    free_workplaces = FreeWorkplacesController.get_free_workplaces()
    user = get_authenticated_user()

    UserController.generate_new_token(user)

    telegram_bot_username = current_app.config.get('TELEGRAM_BOT_USERNAME')
    start_token = user.email + ' ' + user.token
    telegram_link = 'https://t.me/' + telegram_bot_username + '?start=' + quote_plus(start_token)
    return make_response(render_template('bouncer/link_telegram.html',
                                         title='Link to Telegram Account',
                                         user=user,
                                         free_workplaces=free_workplaces,
                                         location_name=current_app.config.get('LOCATION_NAME', 'our location'),
                                         application_name=current_app.config.get('APPLICATION_NAME', 'Bouncer Service'),
                                         operator_website=current_app.config.get('OPERATOR_WEBSITE'),
                                         operator_name=current_app.config.get('OPERATOR_NAME'),
                                         operator_email=current_app.config.get('OPERATOR_EMAIL'),
                                         telegram_bot_username=telegram_bot_username,
                                         telegram_link=telegram_link,
                                         start_token=start_token))


@bouncer_bp.route('/confirm/leave', methods=['GET', 'POST'])
@login_required
def confirm_leave():
    free_workplaces = FreeWorkplacesController.get_free_workplaces()
    user = get_authenticated_user()
    record = RecordController.get_active_record(user)

    if not user.is_confirmed:
        flash('Please confirm your email address first!', 'warning')
        return redirect(url_for('bouncer.home'))

    if record is None:
        flash('You didn\'t report that you were there.', 'warning')
        return redirect(url_for('bouncer.home'))

    if request.method == 'POST':
        if request.form['action'] == 'leave':
            RecordController.terminate(record)
            flash('You have been checked out successfully.')
            return redirect(url_for('bouncer.home'))

    return make_response(render_template('bouncer/confirm_leave.html',
                                         title='Confirm: leave',
                                         user=user,
                                         free_workplaces=free_workplaces,
                                         location_name=current_app.config.get('LOCATION_NAME', 'our location'),
                                         application_name=current_app.config.get('APPLICATION_NAME', 'Bouncer Service'),
                                         operator_website=current_app.config.get('OPERATOR_WEBSITE'),
                                         operator_name=current_app.config.get('OPERATOR_NAME'),
                                         operator_email=current_app.config.get('OPERATOR_EMAIL')))


@bouncer_bp.route('/confirm/enter', methods=['GET', 'POST'])
@login_required
def confirm_enter():
    free_workplaces = FreeWorkplacesController.get_free_workplaces()
    user = get_authenticated_user()

    if not user.is_confirmed:
        flash('Please confirm your email address first!', 'warning')
        return redirect(url_for('bouncer.home'))

    if RecordController.has_active_record(user):
        flash('You already reported to be there!', 'warning')
        return redirect(url_for('bouncer.home'))

    if request.method == 'POST':
        if request.form['action'] == 'enter':
            try:
                UserController.start_record(user)
                flash('You are cleared for entry.')
                return redirect(url_for('bouncer.home'))
            except UserNotConfirmedError:
                    flash('Your account is locked until your email address has been confirmed.', 'error')
            except UserBlockedError:
                flash('Your account is blocked. Please contact an administrator.', 'error')
            except NoFreeWorkplaceError:
                flash ('No free workplaces available! Try again later.', 'error')
                return redirect(url_for('bouncer.home'))

    return make_response(render_template('bouncer/confirm_enter.html',
                                         title='Confirm: enter',
                                         user=user,
                                         free_workplaces=free_workplaces,
                                         location_name=current_app.config.get('LOCATION_NAME', 'our location'),
                                         application_name=current_app.config.get('APPLICATION_NAME', 'Bouncer Service'),
                                         operator_website=current_app.config.get('OPERATOR_WEBSITE'),
                                         operator_name=current_app.config.get('OPERATOR_NAME'),
                                         operator_email=current_app.config.get('OPERATOR_EMAIL')))


@bouncer_bp.route('/confirm/cancel', methods=['GET', 'POST'])
@login_required
def confirm_cancel():
    free_workplaces = FreeWorkplacesController.get_free_workplaces()
    user = get_authenticated_user()
    reservation = ReservationController.get_active_reservation(user)

    if not user.is_confirmed:
        flash('Please confirm your email address first!', 'warning')
        return redirect(url_for('bouncer.home'))

    if not reservation:
        flash('You do not have any reservations!', 'warning')
        return redirect(url_for('bouncer.home'))

    if request.method == 'POST':
        if request.form['action'] == 'cancel':
            ReservationController.cancel(reservation)
            flash('Your reservation has been cancelled.')
            return redirect(url_for('bouncer.home'))

    return make_response(render_template('bouncer/confirm_cancel.html',
                                         title='Confirm: cancel reservation',
                                         user=user,
                                         free_workplaces=free_workplaces,
                                         location_name=current_app.config.get('LOCATION_NAME', 'our location'),
                                         application_name=current_app.config.get('APPLICATION_NAME', 'Bouncer Service'),
                                         operator_website=current_app.config.get('OPERATOR_WEBSITE'),
                                         operator_name=current_app.config.get('OPERATOR_NAME'),
                                         operator_email=current_app.config.get('OPERATOR_EMAIL')))

from urllib.parse import quote_plus
from flask import flash, current_app, redirect, render_template, url_for, request, abort, make_response, session

from . import admin_bp
from ..auth import admin_required, is_authenticated, get_authenticated_user
from ..controllers import FreeWorkplacesController, UserController, ReservationController, RecordController
from ..exceptions import NoFreeWorkplaceError, UserNotConfirmedError

@admin_bp.route('/')
@admin_required
def home():
    free_workplaces = FreeWorkplacesController.get_free_workplaces()
    user = get_authenticated_user()
    reservation = ReservationController.get_active_reservation(user)
    record = RecordController.get_active_record(user)

    return make_response(render_template('admin/home.html',
                                            title='[admin] Dashboard',
                                            application_name=current_app.config.get('APPLICATION_NAME', 'Bouncer Service'),
                                            operator_website=current_app.config.get('OPERATOR_WEBSITE'),
                                            operator_name=current_app.config.get('OPERATOR_NAME'),
                                            operator_email=current_app.config.get('OPERATOR_EMAIL'),
                                            user=user,
                                            reservation=reservation,
                                            record=record))


@admin_bp.route('/create_account', methods=['GET', 'POST'])
@admin_required
def create_account():
    errors = None
    if request.method == 'POST':
        try:
            UserController.check_user_data(
                name=request.form['name'],
                email=request.form['email'],
                ignore_password=True)
            UserController.create_admin(
                name=request.form['name'],
                email=request.form['email'])

            flash('Account for ' + request.form['email'] + ' created.')

        except UserRegistrationInvalidDataError as e:
            errors = e.errors
            flash('Some fields have errors: ' + ', '.join(errors), 'error')
    return make_response(render_template('admin/create_account.html',
                                         title='[admin] Create Account',
                                         errors=errors,
                                         operator_website=current_app.config.get('OPERATOR_WEBSITE'),
                                         operator_name=current_app.config.get('OPERATOR_NAME'),
                                         operator_email=current_app.config.get('OPERATOR_EMAIL')))

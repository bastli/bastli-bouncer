# User-defined exceptions

class Error(Exception):
    """Base class for other exceptions"""
    pass


class NoFreeWorkplaceError(Error):
    """Raised when no free workplaces are available"""
    pass


class ReservationExpiredError(Error):
    """Raised when the reservation has expired"""
    pass


class ActiveReservationExistsError(Error):
    """Raised when there is already an active reservation
    for the given user."""
    pass


class NoActiveReservationExistsError(Error):
    """Raised when there is no active reservation for the given user."""
    pass

class ActiveRecordExistsError(Error):
    """Raised when there is already an active record
    for the given user."""
    pass


class NoActiveRecordError(Error):
    """Raised when there is no active record for the given user."""


class UserNotConfirmedError(Error):
    """Raised when user has no confirmed email address"""
    pass


class UserBlockedError(Error):
    """Raised when a user account is blocked"""
    pass


class UserRegistrationInvalidDataError(Error):
    """Raised when registration data is invalid"""
    
    def __init__(self, errors):
        super(UserRegistrationInvalidDataError, self).__init__()
        self.errors = errors

    def get_errors(self):
        return self.errors


class InvalidUserTokenError(Error):
    """Raised when an invalid user token is given."""


class InvalidCredentialsError(Error):
    """Raised when individual credentials were provided."""


class TelegramUserAlreadyLinkedError(Error):
    """Raised when trying to link a telegram user with is already assigned to an account."""

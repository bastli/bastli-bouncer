from app import db
from datetime import datetime, timedelta

class Record(db.Model):
    """
    Record entry.
    """

    __tablename__ = 'records'

    _id = db.Column(db.Integer, primary_key=True)
    time_start = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    time_end = db.Column(db.DateTime, nullable=True)

    # relation to user
    user_id = db.Column(db.Integer, db.ForeignKey('users._id'))
    user = db.relationship('User', back_populates='records')


    @property
    def is_active(self):
        return self.time_end is None or self.time_end - datetime.now() > timedelta(seconds=0)

import secrets
from flask import current_app
from datetime import datetime, timedelta
from werkzeug.security import generate_password_hash, check_password_hash
from app import db

class User(db.Model):
    """
    User object.
    """

    __tablename__ = 'users'

    _id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), nullable=False, unique=True)
    is_confirmed = db.Column(db.Boolean, nullable=False, default=False)
    is_blocked = db.Column(db.Boolean, nullable=False, default=False)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)
    telegram_id = db.Column(db.String(128), nullable=True, unique=True)
    telegram_chat_id = db.Column(db.String(128), nullable=True)
    password_hash = db.Column(db.String(128), nullable=True)
    token = db.Column(db.String(32), nullable=False)
    token_expiration = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    created = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    reservations = db.relationship("Reservation", back_populates="user")
    records = db.relationship("Record", back_populates="user")


    @property
    def is_telegram_user(self):
        return self.telegram_id is not None


    @property
    def has_password(self):
        return self.password_hash is not None


    def set_password(self, password):
        self.password_hash = generate_password_hash(password)


    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


    def is_token_valid(self, token):
        return self.token == token and \
            self.token_expiration - datetime.now() > timedelta(seconds=0)

    
    def generate_new_token(self):
        self.token = secrets.token_urlsafe(16)
        self.token_expiration = datetime.now() + current_app.config.get('USER_TOKEN_TIMEOUT')

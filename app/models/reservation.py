from app import db
from datetime import datetime, timedelta

class Reservation(db.Model):
    """
    Workplace reservation entry.
    """

    __tablename__ = 'reservations'

    _id = db.Column(db.Integer, primary_key=True)
    time_start = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    time_end = db.Column(db.DateTime, nullable=False)

    # relation to user
    user_id = db.Column(db.Integer, db.ForeignKey('users._id'))
    user = db.relationship('User', back_populates='reservations')


    @property
    def is_valid(self):
        return self.time_end - datetime.now() > timedelta(seconds=0)

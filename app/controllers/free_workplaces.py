import threading
from datetime import datetime
from sqlalchemy import DateTime, cast, func, or_
from flask import current_app
from app.models import Record, Reservation
from app import db
from .lock import Lock
from ..exceptions import NoFreeWorkplaceError

class FreeWorkplacesController():
    workplaces_lock = Lock.workplaces_lock


    @classmethod
    def has_free_workplaces(cls):
        with cls.workplaces_lock:
            return cls._has_free_workplaces()


    @classmethod
    def get_free_workplaces(cls):
        with cls.workplaces_lock:
            return cls._get_free_workplaces()


    @classmethod
    def check_free_workplaces(cls):
        if not cls._has_free_workplaces:
            raise NoFreeWorkplaceError


    # ---------- PRIVATE METHODS BELOW ----------

    @classmethod
    def _get_free_workplaces(cls):
        total_workplaces = current_app.config.get('TOTAL_WORKPLACES')
        now = datetime.now()

        active_records_count = db.session.query(func.count(Record._id)) \
            .filter(cast(Record.time_start,DateTime) <= now) \
            .filter(or_(cast(Record.time_end,DateTime) >= now, Record.time_end == None)) \
            .scalar()

        valid_reservations_count = db.session.query(func.count(Reservation._id)) \
            .filter(cast(Reservation.time_start,DateTime) <= now) \
            .filter(cast(Reservation.time_end,DateTime) >= now) \
            .scalar()


        # check total of active records and valid reservations.
        return total_workplaces - active_records_count - valid_reservations_count

    @classmethod
    def _has_free_workplaces(cls):
        return cls._get_free_workplaces() > 0
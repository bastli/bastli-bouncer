import threading
from datetime import datetime, timedelta
from flask import current_app
from sqlalchemy import DateTime, cast, func, or_
from bot.reminders import remove_reservation_reminder, add_record_reminder, remove_record_reminder
from app import db
from app.models import Record
from .lock import Lock
from .free_workplaces import FreeWorkplacesController
from ..exceptions import ReservationExpiredError, UserNotConfirmedError, UserBlockedError

class RecordController():
    workplaces_lock = Lock.workplaces_lock


    @classmethod
    def create(cls, user):
        with cls.workplaces_lock:
            if not user.is_confirmed:
                raise UserNotConfirmedError
            if user.is_blocked:
                raise UserBlockedError

            FreeWorkplacesController.check_free_workplaces()
            record = Record()
            record.user = user
            record.time_start = datetime.now()
            record.time_end = datetime.now() + current_app.config.get('RECORD_TIMEOUT', timedelta(hours=12))

            db.session.add(record)
            db.session.commit()

            add_record_reminder(record)


    @classmethod
    def create_from_reservation(cls, reservation):
        with cls.workplaces_lock:
            if not reservation.user.is_confirmed:
                raise UserNotConfirmedError
            if reservation.user.is_blocked:
                raise UserBlockedError

            if reservation.is_valid:
                remove_reservation_reminder(reservation)

                time_start = datetime.now()
                reservation.time_end = time_start

                record = Record()
                record.user = reservation.user
                record.time_start = time_start
                record.time_end = time_start + current_app.config.get('RECORD_TIMEOUT', timedelta(hours=12))

                db.session.add(record)
                db.session.commit()

                add_record_reminder(record)
            else:
                raise ReservationExpiredError


    @staticmethod
    def get(record_id):
        return Record.query.get(record_id)


    @classmethod
    def has_active_record(cls, user):
        return cls.get_active_record(user) is not None


    @staticmethod
    def get_active_record(user):
        now = datetime.now()
        return Record.query \
            .filter(cast(Record.time_start,DateTime) <= now) \
            .filter(or_(cast(Record.time_end,DateTime) > now, Record.time_end == None)) \
            .filter(Record.user_id == user._id) \
            .first()


    @staticmethod
    def get_all_active_records():
        now = datetime.now()
        return Record.query \
            .filter(cast(Record.time_start,DateTime) <= now) \
            .filter(or_(cast(Record.time_end,DateTime) > now, Record.time_end == None)) \
            .all()


    @classmethod
    def initialize_notifications(cls):
        records = cls.get_all_active_records()

        for record in records:
            add_record_reminder(record)


    @classmethod
    def terminate(cls, record):
        with cls.workplaces_lock:
            record.time_end = datetime.now()
            db.session.commit()

            remove_record_reminder(record)

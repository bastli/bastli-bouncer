import json
from functools import wraps
from flask import request, redirect, abort, session, flash
from app import db
from .models import User
from .exceptions import UserBlockedError, InvalidCredentialsError


def login_required(f):
    """
    Requires that the user is logged in.
    """
    @wraps(f)
    def wrapped(*args, **kwargs):
        if not is_authenticated():
            abort(401)

        return f(*args, **kwargs)
    return wrapped


def admin_required(f):
    """
    Requires that the user is logged in and has admin privileges
    """
    @wraps(f)
    def wrapped(*args, **kwargs):
        if not is_authenticated():
            abort(401)
        elif not is_admin():
            abort(403)

        return f(*args, **kwargs)
    return wrapped


def is_authenticated():
    if 'userID' in session and session['userID'] is not None:
        user = User.query.get(session['userID'])
        return user and not user.is_blocked
    return False


def is_admin():
    if 'userID' in session and session['userID'] is not None:
        user = User.query.get(session['userID'])
        return user is not None and user.is_admin
    return False


def get_authenticated_user():
    if 'userID' in session and session['userID'] is not None:
        user = User.query.get(session['userID'])
        if user and not user.is_blocked:
            return user
    return None


def login(email, password):
    if email is None or password is None:
        return False

    user = User.query.filter(User.email == email).first()

    if user and user.check_password(password):
        if user.is_blocked:
            raise UserBlockedError
        session['userID'] = user._id
    else:
        raise InvalidCredentialsError


def logout():
    session['userID'] = None

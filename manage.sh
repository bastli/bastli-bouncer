#!/bin/bash

# set current directory to the script location
cd "$(dirname "$0")"

if [[ $EUID -eq 0 || $(groups | grep "\bdocker\b") ]]; then
    SUDO_COMMAND=""
else
    SUDO_COMMAND="sudo"
fi

project_dir_name=${PWD##*/}

PROD_DOCKER_RUN_COMMAND="${SUDO_COMMAND} docker run -it \
    -p 8080:8080 \
    --network ${project_dir_name}_backend \
    -v ${PWD}:/bastlibouncer \
    -v ${PWD}/instance/config.dev.py:/bastlibouncer/instance/config.py \
    -v /etc/localtime:/etc/localtime \
    bastlibouncer"

BASE_DOCKER_RUN_COMMAND="${SUDO_COMMAND} docker run -it \
    -p 5000:5000 \
    --network ${project_dir_name}_backend \
    -v ${PWD}:/bastlibouncer \
    -v ${PWD}/instance/config.dev.py:/bastlibouncer/instance/config.py \
    -v /etc/localtime:/etc/localtime \
    bastlibouncer-dev"

case $1 in
    build)
        case $2 in
            dev)
                $SUDO_COMMAND docker build -t bastlibouncer-dev -f Dockerfile.development .
                ;;
            prod)
                $SUDO_COMMAND docker build -t bastlibouncer -f Dockerfile .
                ;;
            *)
                echo "Unknown variant for command \"build\"."
                echo "Available variants: prod, dev"
                exit 1
                ;;
        esac
        ;;
    run)
        case $2 in
            dev)
                $BASE_DOCKER_RUN_COMMAND flask run --host=0.0.0.0
                ;;
            prod)
                $PROD_DOCKER_RUN_COMMAND
                ;;
            *)
                echo "Unknown variant for command \"run\"."
                echo "Available variants: prod, dev"
                exit 1
                ;;
        esac
        ;;
    services)
        case $2 in
            start)
                $SUDO_COMMAND docker-compose up -d
                ;;
            restart)
                $SUDO_COMMAND docker-compose restart db
                ;;
            stop)
                $SUDO_COMMAND docker-compose stop
                ;;
            *)
                echo "Unknown sub-command for command \"services\"."
                echo "Available sub-commands: start, restart, stop"
                exit 1
                ;;
        esac
        ;;
    makemigrations)
        $BASE_DOCKER_RUN_COMMAND flask db migrate
        ;;
    migrate)
        $BASE_DOCKER_RUN_COMMAND flask db upgrade
        ;;
    inspect)
        $SUDO_COMMAND docker exec -it "${project_dir_name}_db_1" mysql -u bastlibouncer -pbastlibouncer bastlibouncer
        if [ $? -ne 0 ]; then
            echo "Someting did not work as expected!"
            echo "Your database seems to have a different name or username/password."
        fi
        ;;
    update_dependencies)
        $BASE_DOCKER_RUN_COMMAND pip-compile --output-file requirements.txt requirements.in
        ;;
    *)
        echo "Management Script Usage:"
        echo ""
        echo "  manage.sh [COMMAND]"
        echo ""
        echo "COMMAND:"
        echo "  build [dev|prod]               Build docker image for given environment."
        echo "  run [dev|prod]                 Run docker container for given environment."
        echo "  services [start|restart|stop]  Start/stop service dependencies."
        echo "  makemigrations                 Create new migration files."
        echo "  migrate                        Apply migrations to local database."
        echo "  inspect                        Inspect database with mysql cli."
        echo "  update_dependencies            Update dependencies based in requirements.in."
        ;;
esac

# Local Development Configuration File
from datetime import timedelta

TOTAL_WORKPLACES = 3
RESERVATION_DURATION = timedelta(hours=1)
RECORD_TIMEOUT = timedelta(hours=13)
USER_TOKEN_TIMEOUT = timedelta(hours=4)

RECORD_REMINDER_INITIAL_DELAY = timedelta(hours=4)
RECORD_REMINDER_INTERVAL = timedelta(hours=2)
RESERVATION_REMINDER_TIME_DELTA = timedelta(minutes=11)

REGISTRATION_ENABLED = False
ACCOUNT_UPDATE_ALLOWED = False

# For production image
# SERVER_NAME = 'localhost:8080'
# For development image
SERVER_NAME = 'localhost:5000'

MAIL_SERVER = 'smtp'
MAIL_PORT = 1025
MAIL_USE_TLS = False
MAIL_USERNAME = None
MAIL_PASSWORD = None
MAIL_DEFAULT_SENDER = ('Bastli Bouncer', 'noreply@bastli.ethz.ch')

# This is the configuration for @bastli_bouncer_test_bot only!
TELEGRAM_BOT_TOKEN = '1160725393:AAE702dNXaXaLHccHHd8wAg9b50_tuUeAcY'
TELEGRAM_BOT_USERNAME = 'bastli_bouncer_dev_bot'
TELEGRAM_BOT_NAME = 'Bastli Bouncer bot'

APPLICATION_NAME = 'Bastli Bouncer'
LOCATION_NAME = 'Bastli'

OPERATOR_WEBSITE = 'https://bastli.ethz.ch'
OPERATOR_NAME = 'AMIV Bastli'
OPERATOR_EMAIL = 'it@bastli.ethz.ch'

DEBUG = True
SQLALCHEMY_TRACK_MODIFICATIONS = False

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://bastlibouncer:bastlibouncer@db/bastlibouncer'
SECRET_KEY = 'f08d502a04cfc9d945184f12f577f3ad'

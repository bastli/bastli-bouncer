# Example Configuration File
from datetime import timedelta

TOTAL_WORKPLACES = 3
RESERVATION_DURATION = timedelta(hours=1)
RECORD_TIMEOUT = timedelta(hours=13)
USER_TOKEN_TIMEOUT = timedelta(hours=12)

RECORD_REMINDER_INITIAL_DELAY = timedelta(hours=4)
RECORD_REMINDER_INTERVAL = timedelta(hours=2)
RESERVATION_REMINDER_TIME_DELTA = timedelta(minutes=11)

REGISTRATION_ENABLED = True
ACCOUNT_UPDATE_ALLOWED = True

SERVER_NAME = 'bouncer.bastli.ethz.ch'

MAIL_SERVER = 'localhost'
MAIL_PORT = 25
MAIL_USE_TLS = True
MAIL_USERNAME = None
MAIL_PASSWORD = None
MAIL_DEFAULT_SENDER = 'Bastli Bouncer <noreply@bastli.ethz.ch>'

TELEGRAM_BOT_TOKEN = '<telegram-bot-token>'
TELEGRAM_BOT_USERNAME = '<telegram-bot-username>'
TELEGRAM_BOT_NAME = 'Bastli Bouncer bot'

APPLICATION_NAME = 'Bastli Bouncer'
LOCATION_NAME = 'Bastli'

OPERATOR_WEBSITE = 'https://bastli.ethz.ch'
OPERATOR_NAME = 'AMIV Bastli'
OPERATOR_EMAIL = 'it@bastli.ethz.ch'

DEBUG = False
SQLALCHEMY_TRACK_MODIFICATIONS = False

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://<username>:<password>@<host>/<db-name>'
SECRET_KEY = 'replace me (24 random bytes)'
